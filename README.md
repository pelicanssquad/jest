# Jest

## 1. Install `yarn`

Using `apt` or `pacman` or whatever

## 2. Clone

Obviously 🙄 😂

## 3. Install packages

```bash
cd jest
yarn install
```

## 4. Launch tests

`yarn test` or `yarn watch:test` if you want a watcher